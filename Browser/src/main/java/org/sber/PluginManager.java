package org.sber;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginManager {

    private final String pluginRootDirectory;


    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }


    public  Plugin load(String pluginName, String pluginClassName) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String pluginNameAbsolutePath = Paths.get(pluginRootDirectory).resolve(pluginName).toString();  // создаем полный путь
        Class classFromJarFile = getClassFromJarFile(pluginNameAbsolutePath, pluginClassName);
        return (Plugin) classFromJarFile.getConstructor().newInstance();

    }

    // Получаем сэт из имен всех классов, которые есть в jar файле
    private static Set<String> getClassNamesFromJarFile(String pluginNameAbsolutePath) throws IOException {
        Set<String> classNames = new HashSet<>();
        try (JarFile jarFile = new JarFile(new File(pluginNameAbsolutePath))) {
            Enumeration<JarEntry> e = jarFile.entries();
            while (e.hasMoreElements()) {
                JarEntry jarEntry = e.nextElement();
                if (jarEntry.getName().endsWith(".class")) {
                    String className = jarEntry.getName()
                            .replace("/", ".")
                            .replace(".class", "");
                    classNames.add(className);
                }
            }
            return classNames;
        }
    }

    // проходимся по сэту имен классов , выбираем нужный класс и загружаем его через URLClassLoader
    public static Class getClassFromJarFile(String pluginNameAbsolutePath, String pluginClassName) throws IOException, ClassNotFoundException {
        Set<String> classNames = getClassNamesFromJarFile(pluginNameAbsolutePath);
        try (URLClassLoader cl = URLClassLoader.newInstance(new URL[]{new URL("jar:file:" + pluginNameAbsolutePath + "!/")})) {
            for (String name : classNames) {
                String nameForFind = name.substring(name.lastIndexOf('.') +1, name.length());
                if (nameForFind.equals(pluginClassName)) {
                    Class clazz = cl.loadClass(name);
                    return clazz;
                }
            }
        }
        return null;
    }
}
