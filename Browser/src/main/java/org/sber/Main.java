package org.sber;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        PluginManager pluginManager = new PluginManager("D:\\Sber\\jar");
        Plugin person1 = pluginManager.load("Plugin1-1.0-SNAPSHOT.jar", "Person");
        person1.doUsefull();

        Plugin person2 = pluginManager.load("Plugin2-1.0-SNAPSHOT.jar", "Person");
        person2.doUsefull();
    }
}
